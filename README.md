README for PredictCancerType
Author: Wytze Bekker
Date: 16 November 2020

This package can be used to predict the subtype of breastcancer based on 30 attributes. 
To predict for multiple instances:
java -jar build/libs/wrapperthema09-1.0-SNAPSHOT-all -f (filename)

To predict for one instance:
java -jar build/libs/wrapperthema09-1.0-SNAPSHOT-all -i (instance as string, no patient ID)

To see the order the attributes should be in for a single instance:
java -jar build/libs/wrapperthema09-1.0-SNAPSHOT-all -o

To see the help:
java -jar build/libs/wrapperthema09-1.0-SNAPSHOT-all (no arguments)
or
java -jar build/libs/wrapperthema09-1.0-SNAPSHOT-all -h

