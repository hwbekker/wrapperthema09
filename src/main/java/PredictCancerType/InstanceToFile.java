package PredictCancerType;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.*;

//Recieve instance from PredictorApp, copy templatefile under new name, append the instance, save overwrite, give back to PredictorApp
public class InstanceToFile {
    public void start(String inputInstance) {
        String instanceFile = "data/instanceFile.arff";
        String attributesTemplate = "data/ARFFtemplate.arff";
        System.out.println("inputinstance = " + inputInstance);

        InstanceToFile instanceToFile = new InstanceToFile();
        instanceToFile.copyFile(attributesTemplate, instanceFile);
        instanceToFile.writeToFile(instanceFile, inputInstance);
    }



    private void copyFile(String attributesTemplate, String instanceFile){
        Path path = Paths.get(attributesTemplate);
        try {
            Path copy = Paths.get(instanceFile);
            Files.copy(path, copy, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeToFile(String instanceFile, String inputInstance) {
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(instanceFile), StandardOpenOption.APPEND))
        {
            writer.write("?," + inputInstance);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
