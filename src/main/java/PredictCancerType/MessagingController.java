package PredictCancerType;

public class MessagingController {
    private final OptionsProvider optionsProvider;
    public MessagingController(final OptionsProvider optionsProvider) {
        this.optionsProvider = optionsProvider;
    }
    public void start() {
        if (optionsProvider == null) {
            throw new IllegalStateException("No optionscontroller");
        } else {
            printUserSettings();
        }
        System.out.println("Done");

    }
    private void printUserSettings() {
        String inputFile = optionsProvider.getInputDataFile();
        System.out.println("The file you put in" + inputFile);
    }
}
