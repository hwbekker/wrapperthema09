package PredictCancerType;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ApacheCliOptionsProvider implements OptionsProvider {
    private static final String INPUTFILE = "inputfile";
    private static final String INSTANCE = "instance";
    private static final String HELP = "help";
    private static final String ATTRIBUTEORDER = "attributeorder";

    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;
    private String inputfile;
    private String instance;

    public ApacheCliOptionsProvider(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    public void initialize() {
        buildOptions();
        processCommandLine();
    }

    public boolean helpRequested() {
        return this.commandLine.hasOption(HELP);
    }

    public boolean instanceGiven() {
        return this.commandLine.hasOption(INSTANCE);
    }

    public boolean fileGiven() {
        return this.commandLine.hasOption(INPUTFILE);
    }

    public boolean attributeOrderRequested() {return this.commandLine.hasOption(ATTRIBUTEORDER); }

    private void buildOptions() {
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Shows the help, also shows when no arguments are given");
        Option fileOption = new Option("f", INPUTFILE, true, "the file you would like to be classified");
        Option instanceOption = new Option("i", INSTANCE, true, "a single instance you would like to be classified. " +
                "\n The order in which these attribute should be can be found using -a. " +
                "\n Do not add patient ID as first attribute");
        Option attributeOrder = new Option("a", ATTRIBUTEORDER, false, "Shows the order the attributes should be in");

        options.addOption(helpOption);
        options.addOption(fileOption);
        options.addOption(instanceOption);
        options.addOption(attributeOrder);
    }

    private void processCommandLine() {
        try{
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);
            if (commandLine.hasOption(INPUTFILE)) {
                if(isLegalDatafile(commandLine.getOptionValue(INPUTFILE))) {
                    System.out.println("Classifying " + this.commandLine.getOptionValue(INPUTFILE));
                    this.inputfile = commandLine.getOptionValue(INPUTFILE);
                } else {
                    throw new IllegalArgumentException("Datafile not valid, please give .arff file");
                }
            } else if (commandLine.hasOption(INSTANCE)) {
                if(isLegalInstance(this.commandLine.getOptionValue(INSTANCE))) {
                    System.out.println("Classifying " + this.commandLine.getOptionValue(INSTANCE));
                    this.instance = commandLine.getOptionValue(INSTANCE);
                } else {
                    throw new IllegalArgumentException("Instance not valid.");
                }
            }

        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("PredictorApp", options);
    }

    public void printAttHelp() throws IOException {
        BufferedReader r = new BufferedReader( new FileReader( "data/AttributeOrder" ) );
        String s = "", line = null;
        while ((line = r.readLine()) != null) {
            s += line + "\n";
        }
        System.out.print(s);

    }

    private boolean isLegalDatafile(String datafile) {
        try {
            if(datafile.endsWith(".arff")) {
                return true;
            }
        } catch (Exception ex) {
            System.out.println("Filetype not valid");
        }
        return false;
    }


    private boolean isLegalInstance(String i) {
        try {
            String[] instanceValues = commandLine.getOptionValue(INSTANCE).split(",");
            if(instanceValues.length == 29) {
                return true;
            } else {
                throw new IllegalArgumentException("Instance has incorrect amount of values, should have 29 values.");
            }
        } catch (Exception ex) {
            System.out.println();
            return false;
        }
    }


    @Override
    public String getInputDataFile() {
        return this.inputfile;
    }

    @Override
    public String getInstance() {
        return this.instance;
    }
}
