package PredictCancerType;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UserInputOptionsProvider implements OptionsProvider{
    private String inputDataFile;

    public UserInputOptionsProvider() {
        initialize();
    }
    private void initialize() {
        Scanner scanner = new Scanner(System.in);
        fetchDataFile(scanner);
    }

    private void fetchDataFile(final Scanner scanner) {
        String dataFile = "";
        System.out.println("Enter the name of the arff datafile to be classified");
        try {
            dataFile = scanner.next();
        } catch (InputMismatchException ex) {
            System.out.println("Input unvalid");
            System.exit(0);
        }
        this.inputDataFile = dataFile;
    }

    @Override
    public String getInputDataFile() {
        return inputDataFile;
    }

    @Override
    public String getInstance() {
        return null;
    }
}

