package PredictCancerType;

import java.io.IOException;

public class PredictorApp {
    public static void main(String[] args) throws IOException {
        ApacheCliOptionsProvider ACOP = new ApacheCliOptionsProvider(args);

            //ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(args);
            if (args.length == 0) {
                ACOP.printHelp();
            }
            if (ACOP.helpRequested()) {
                ACOP.printHelp();
            }
            if (ACOP.attributeOrderRequested()){
                ACOP.printAttHelp();
            }
            if (ACOP.instanceGiven()) {
                //Write instance to template file
                String instanceFile = "data/instanceFile.arff";
                InstanceToFile instanceToFile = new InstanceToFile();
                instanceToFile.start(ACOP.getInstance());
                //Use new file on Wekarunner
                WekaRunner wekaRunner = new WekaRunner(instanceFile);
                wekaRunner.start();



            }
            if (ACOP.fileGiven()) {
                //Use file on WekaRunner
                WekaRunner wekarunner = new WekaRunner(ACOP.getInputDataFile());
                wekarunner.start();
            }

    }

}
