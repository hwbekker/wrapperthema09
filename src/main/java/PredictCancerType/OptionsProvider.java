package PredictCancerType;

public interface OptionsProvider {
    String getInputDataFile();
    String getInstance();
}
