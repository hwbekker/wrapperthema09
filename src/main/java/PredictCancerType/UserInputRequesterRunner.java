package PredictCancerType;

public final class UserInputRequesterRunner {
    private UserInputRequesterRunner() { }

    public static void main(final String[] args) {
        OptionsProvider op = new UserInputOptionsProvider();
        MessagingController controller = new MessagingController(op);
        controller.start();
    }

}
